todo
------

**todo** is a minimalistic to-do manager for the shell. It supports:

- auto-archiving completed tasks with datetime stamp
- regular expressions support for task completion
- command-completion
- `tmux` session name integration

_Development based on [this BASH function](https://github.com/jamestomasino/dotfiles/blob/master/bash/.functions/todo)_

(in development)

